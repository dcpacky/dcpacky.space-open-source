from django import forms
from captcha.fields import CaptchaField
from .models import Post, Comment, adminMessage, ProgrammingSection


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('title', 'text', 'materialize_icon', )
        
        
class CommentForm(forms.ModelForm):
    captcha = CaptchaField()
    class Meta:
        model = Comment
        fields = ('author', 'text')
      
    
class MessageToAdminForm(forms.ModelForm):
    captcha = CaptchaField()
    class Meta:
        model = adminMessage
        fields = ('title', 'byName', 'text', 'email')
        

class ProgrammingSectionForm(forms.ModelForm):
    class Meta:
        model = ProgrammingSection
        fields = ('title', 'description', 'link')