## DcPacky.Space Open Source Release

#### This is the public Open Source release of the DcPacky.Space Blog.

It's licensed using the GNU General Public License v3.0 (GNU GPLv3)

[License Information provided by ChooseALicense.com](https://choosealicense.com/licenses/gpl-3.0/)
***

For a live example visit: [DcPacky.Space](https://dcpacky.space/)

(Mind that there may be minor changes from the Live Version and the Open Source release)

> If you have the feel that [DcPacky.Space](https://dcpacky.space/) had way more updates than this project, contact me please using the methods described at the very bottom of this page, then I'd only forgot to update the Open Source release

***

Needs Python 3.5+ and the following dependencies

+ django
+ django-simple-captcha
> Those are already provided by the VENV in the package, in case you want to use your own python venv or installation you need to install those packages using PIP

*** 
### Installation Instructions for Local Development

> In it's standard configuration the blog uses SQLite as a database use the settings.py to change this if you want
+ When you're in the project directory use the VENV to call the makemigrations command of the manage.py file 
```
$ ./venv/bin/python3.6 ./manage.py makemigrations
```
+ Next do the same again using the migrate command, it's configuring the database accordingly
```
$ ./venv/bin/python3.6 ./manage.py migrate
```
+ Collect all static files using
```
$ ./venv/bin/python3.6 ./manage.py collectstatic
```
+ Now you can start the testserver using the runserver command
```
$ ./venv/bin/python3.6 ./manage.py runserver
```

+ Finally create a superuser for your blog, since the website has no account creation, as currently intended (Only creators of blog content should have an account, but this can also be easily modified)
```
$ ./venv/bin/python3.6 ./manage.py createsuperuser
```

> To publish your website check out [Deployment checklist | Django documentation | Django](https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/) or if you are using Pythonanywhere, they also have a great documentation on how to install your project

### Things you might want to change before publishing
+ Add Google Analytics
> Easiest method is to paste your Google Analytics Code into '/blog/templates/blog/base.html' to the line 34 | Alternatively there is an Analytics Django Package but i have no experience with it

+ Change the constant content on the website, to do this change the text on the following lines
    - Line 13 - 29 in /blog/templates/blog/base.html (Favicon)
    - Line 267 in /blog/templates/blog/base.html (Replace the Discord Server widget with your own, or remove it)
    - Line 154 in /blog/templates/blog/base.html (Short explaining text on the header)
    - Line 23 in /blog/templates/blog/post_list.html (Bit longer descriptive text on the frontpage)
    - Line 8,9,15 in /blog/templates/blog/twitter.html (Replace it with your Twitter embed code, or remove page)
    - Line 15 in /blog/templates/blog/programming.html (Explaining text about the programming page, or whatever you'll be using it)
    - Line 8 in /blog/templates/blog/about.html (Add your legal notice, or other important information here)
    - Line (279 - 284 && 226 - 231 && 156 - 161)  in /blog/templates/blog/base.html (Social Media links)
    - Use your own pictures, base.html(150,169,170), post_list.html(18), programming.html(10)
    
> Most of those things will be directly implemented into the Django models, in the future

***
### Known Bugs or planned updates
+ Bugs
    - Image in block on post_list.html & programming.html, doesn't stretch and leaves a white background on some resolutions
    - On a very small resolution the links in the footer may bleed into the discord widget
    - Links in footer are still to the middle aligned, even on smaller screens
    - Sidenav is deactivated on screens smaller 992px to prevent problems that had occurred using the custom JS
    
+ ToDo:
    - Move the constant content mentioned above to models.
    - Rework and Outsource JS

***
    
Feel free to contact me using the following methods
+ E-Mail: [dcpacky@protonmail.com](mailto:dcpacky@protonmail.com)
+ Contact Formular: [DcPacky.Space|Contact](https://dcpacky.space/admin_contact)
+ Twitter: [DcPacky(@dcpacky@)|Twitter](https://twitter.com/dcpacky)

You can find other methods like Discord on my website [DcPacky.Space](https://dcpacky.space/)